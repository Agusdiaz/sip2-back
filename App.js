var express = require('express')
var bodyParser = require('body-parser');
var cors = require('cors');
var app = express();

// Import router
var apiRoutes = require("./ApiRoutes")

// Todo lo que recibe la app se tratara como json
app.use(bodyParser.urlencoded(
    {
        extended: true
    }));
app.use(bodyParser.json());
app.use(cors());

var port = process.env.PORT || 8080;

app.get('/', (req, res) => res.send('Hello World with Express'));

// Use Api routes in the App
app.use('/rutes', apiRoutes);

app.listen(port, function () {
    console.log("Running RestHub on port " + port);
});