var iniciativas = require('../modelos/iniciativas');
var bodyParser = require('body-parser');

let insertIniciativa = (req, res) => {
    var newIniciativa = iniciativas({
        titulo: req.body.titulo,
        geoLocalizacion: req.body.geoLocalizacion,
        fechaLimite: req.body.fechaLimite,
        categoria: req.body.categoria,
        descripcion: req.body.descripcion,
        email: req.body.email,
    });
    newIniciativa.save().
        then
        (
            res.send('Iniciativa guardada'),
            (err) => { console.log(err); }
        )
}

let getIniciativasByCategoria = (req, res) => {
    let categoria = { categoria: req.body.categoria };
    iniciativas.find(categoria)
        .then
        (
            (listaIniciativas) => {
                res.send(listaIniciativas);
            },
            (err) => { console.log(err); }
        )
};

module.exports = { insertIniciativa, getIniciativasByCategoria };