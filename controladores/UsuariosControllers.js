var usuarios = require('../modelos/usuarios');
var bodyParser = require('body-parser');
var comentarios = require('./IniciativasControllers');

let insertUsuario = (req, res) => {
    let idBusqueda = {
        email: req.body.email,
    };
    usuarios.findOne(idBusqueda)
        .then
        (
            (usuario) => {
                if (usuario == null || usuario == undefined) {
                    usuario = usuarios({
                        email: req.body.email,
                        password: req.body.password,
                        nombre: req.body.nombre,
                        apellido: req.body.apellido,
                        fechaNacimiento: req.body.fechaNacimiento,
                        colaborador: req.body.colaborador,
                        impulsor: req.body.impulsor                       
                    });
                    usuario.save().
                        then
                        (
                            res.send('Usuario guardado'),
                            (err) => { console.log(err); }
                        )
                } else {
                    res.send('Usuario ya existe con ese email');
                }
            },
            (err) => { console.log(err); }
        )
}

let getUsuarios = (res) => {
    usuarios.find()
        .then
        (
            (listaUsuarios) => res.send(listaUsuarios),
            (err) => { console.log(err); }
        )
};

let login = (req, res) => {
    let idBusqueda = {
        email: req.body.email,
        password: req.body.password
    };
    usuarios.findOne(idBusqueda)
        .then
        (
            (usuario) => {
                res.send(usuario);  
                console.log(usuario);
            },
            (err) => { console.log(err); }
        )
};

let getUsuarioByMail = (req, res) => {
    let idBusqueda = {
        email: req.body.email,
    };
    usuarios.findOne(idBusqueda)
        .then
        (
            (usuario) => {
                res.send(usuario);  
            },
            (err) => { console.log(err); }
        )
};

let setPassword = (req, res) => {
    let idBusqueda = {
        email: req.body.email,
    };
    let update = {
        password: req.body.password,
    };
    usuarios.updateOne(idBusqueda, update)
        .then
        ((rta) => {
            if (rta.n === 1)
                res.send('Password cambiada');
            else
                res.send('Usuario incorrecto');
        }, (err) => { console.log(err); })
};

module.exports = { insertUsuario, login, getUsuarios, getUsuarioByMail, setPassword }; 