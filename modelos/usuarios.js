var connection = require('../mongooseConnection');

var Schema = connection.Schema;

var usuarioSchema = new Schema({
    email: String,
    password: String,
    nombre: String,
    apellido: String,
    fechaNacimiento: Date,
    colaborador: Boolean,
    impulsor: Boolean    
});

var Usuarios = connection.model('Usuario', usuarioSchema);

module.exports = Usuarios;