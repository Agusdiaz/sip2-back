var connection = require('../mongooseConnection');

var iniciativaSchema = connection.Schema({
    titulo: String,
    geoLocalizacion: String,
    fechaLimite: Date,
    categoria: String,
    descripcion: String, 
    email: String,
});

var Iniciativas = connection.model('Iniciativa', iniciativaSchema);

module.exports = Iniciativas;