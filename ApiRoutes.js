let router = require('express').Router();
let IniciativasController = require('./controladores/IniciativasControllers');
let UsuariosController = require('./controladores/UsuariosControllers');

const paramUsr = '/:usuario';
const paramPass = '/:pass';
const paramId = '/:id';
const paramMail = '/:mail';

router.get('/', function (req, res) {
    res.json(
        {
            status: 'API Its Working',
            message: 'Welcome to RESTHub crafted with love!!!!!!',
        });
});

router.post('/insertIniciativa', function (req, res) {
    IniciativasController.insertIniciativa(req, res);
});

router.post('/getIniciativasByCategoria', function (req, res) {
    IniciativasController.getIniciativasByCategoria(req, res);
});

router.get('/getUsuarios', function (req, res) {
    console.log("getUsuarios");
    UsuariosController.getUsuarios(res);
});

router.post('/login', function (req, res) {
    console.log("login");
    UsuariosController.login(req, res);
});

router.post('/getUsuarioByMail', function (req, res) {
    UsuariosController.getUsuarioByMail(req, res);
});

router.post('/setPassword', function (req, res) {
    UsuariosController.setPassword(req, res);
});

router.post('/insertUsuario', function (req, res) {
    console.log(req.body);
    IniciativasController.insertUsuario(req, res);
});

module.exports = router;